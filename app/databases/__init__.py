# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.databases.mongo_db import MongoDB


class Database(object):

    def __init__(self):
        super(Database, self).__init__()
        # add connections to different dbs here
        self.mongo_db = MongoDB()
