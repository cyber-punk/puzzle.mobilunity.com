# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import mongoengine as mongo_db
from django.test import SimpleTestCase
from django.conf import settings

from app.databases import Database


class BaseTestCase(SimpleTestCase):

    def setUp(self):
        super(BaseTestCase, self).setUp()
        self.db = Database()

    def tearDown(self):
        connection = mongo_db.connect('default')
        connection.drop_database(settings.MONGODB_DATABASES['default']['name'])
        super(BaseTestCase, self).tearDown()
