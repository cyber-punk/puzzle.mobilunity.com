# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url, include

from app.api_tourists.router import router as api_tourists_router
from app.map.urls import urlpatterns as map_urlpatterns


urlpatterns = [
    url(r'^api/v1/', include(api_tourists_router.urls)),
]

urlpatterns += map_urlpatterns
