# -*- coding: utf-8 -*-
# pylint: disable=model-missing-unicode
from __future__ import unicode_literals

import mongoengine as db

from app.common.models.mongo_model import IdMongoModel, MongoEmbededDocument


class GeoPoint(MongoEmbededDocument):
    lon = db.fields.DecimalField(precision=6, min_value=0, max_value=180)
    lat = db.fields.DecimalField(precision=6, min_value=-90, max_value=90)


class Device(MongoEmbededDocument):
    phone = db.StringField(max_length=20)
    imei = db.StringField(max_length=16)
    mac = db.StringField(max_length=17)
    manufacture = db.StringField(max_length=20)
    model = db.StringField(max_length=40)


class Tourist(IdMongoModel):
    first_name = db.StringField(max_length=40)
    last_name = db.StringField(max_length=40)
    device = db.EmbeddedDocumentField(Device)
    location = db.EmbeddedDocumentField(GeoPoint)
    route = db.EmbeddedDocumentListField(GeoPoint)
