# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from rest_framework.routers import SimpleRouter

from app.api_tourists.views import TouristResource


router = SimpleRouter()


router.urls.append(
    url(r'tourists$',
        TouristResource.as_view(),
        name='api_v1__tourist_resource')
)

router.urls.append(
    url(r'tourists/(?P<id>.*)$',
        TouristResource.as_view(),
        name='api_v1__tourist_resource')
)
