# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.common.views import BaseStorageResource
from app.api_tourists.serializers import TouristSerializer
from app.api_tourists.services import TouristStorageService


class TouristResource(BaseStorageResource):
    data_serializer_cls = TouristSerializer
    filter_serializer_cls = TouristSerializer
    storage_cls = TouristStorageService
