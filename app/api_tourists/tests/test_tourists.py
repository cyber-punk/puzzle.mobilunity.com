# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from uuid import uuid4
from decimal import Decimal

from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.serializers.json import DjangoJSONEncoder

from app.tests.base_test_case import BaseTestCase
from app.api_tourists.services import TouristStorageService


class TestTourists(BaseTestCase):

    def setUp(self):
        super(TestTourists, self).setUp()
        self.BARCELONA_LOCATION = {
            'lon': Decimal('41.394898'),
            'lat': Decimal('2.078557')
        }
        self.MADRID_LOCATION = {
            'lon': Decimal('40.438131'),
            'lat': Decimal('-3.819963')
        }
        self.BILBAO_LOCATION = {
            'lon': Decimal('45.364187'),
            'lat': Decimal('-7.398076')
        }
        self.SAMSUNG_GALAXY_NOTE_7 = {
            'phone': '+380679332332',
            'imei': '351554053620648',
            'mac': '80:6C:1B:DB:8D:B7',
            'manufacture': 'Samsung',
            'model': 'Galaxy Note 7'
        }
        self.IPHONE_6 = {
            'phone': '+380677677200',
            'imei': '357332043405725',
            'mac': 'DC:9F:A4:F8:18:49',
            'manufacture': 'Apple',
            'model': 'iPhone 6'
        }
        self.generate_tourists()

    def generate_tourists(self):
        self.tourist_1 = uuid4()
        TouristStorageService.create(dict(
            id=self.tourist_1,
            first_name='George',
            last_name='Orwel',
            location=self.BARCELONA_LOCATION,
            device=self.SAMSUNG_GALAXY_NOTE_7
        ))
        self.tourist_2 = uuid4()
        TouristStorageService.create(dict(
            id=self.tourist_2,
            first_name='Errico',
            last_name='Malatesta',
            location=self.MADRID_LOCATION,
            device=self.IPHONE_6
        ))

    def test_get_tourist_item(self):
        response = self.client.get(
            path=reverse('api_v1__tourist_resource', kwargs={
                             'id': self.tourist_1
                         }),
            headers={'Content-type': 'application/json'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertDictContainsSubset({
            'id': self.tourist_1,
            'first_name': 'George',
            'last_name': 'Orwel'
        }, response.data)
        self.assertDictContainsSubset(
            self.BARCELONA_LOCATION,
            response.data['location']
        )

    def test_get_tourist_list(self):
        response = self.client.get(
            path=reverse('api_v1__tourist_resource'),
            headers={'Content-type': 'application/json'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertDictContainsSubset({
            'limit': settings.MAX_PAGING_LIMIT,
            'offset': 0,
            'total': 2,
            'count': 2
        }, response.data)
        self.assertDictContainsSubset({
            'id': self.tourist_1,
            'first_name': 'George',
            'last_name': 'Orwel'
        }, response.data['items'][0])
        self.assertDictContainsSubset(
            self.BARCELONA_LOCATION,
            response.data['items'][0]['location']
        )
        self.assertDictContainsSubset({
            'id': self.tourist_2,
            'first_name': 'Errico',
            'last_name': 'Malatesta'
        }, response.data['items'][1])
        self.assertDictContainsSubset(
            self.MADRID_LOCATION,
            response.data['items'][1]['location']
        )

    def test_post_tourist_item(self):
        response = self.client.post(
            path=reverse('api_v1__tourist_resource'),
            headers={'Content-type': 'application/json'},
            content_type='application/json',
            data=json.dumps({
                'first_name': 'Emma',
                'last_name': 'Goldman',
                'location': self.BARCELONA_LOCATION,
                'device': self.SAMSUNG_GALAXY_NOTE_7
            }, cls=DjangoJSONEncoder)
        )
        self.assertEqual(response.status_code, 201)
        self.assertDictContainsSubset({
            'first_name': 'Emma',
            'last_name': 'Goldman'
        }, response.data)
        self.assertDictContainsSubset(
            self.BARCELONA_LOCATION,
            response.data['location']
        )
        self.assertDictContainsSubset(
            self.BARCELONA_LOCATION,
            response.data['route'][0]
        )
        self.assertDictContainsSubset(
            self.SAMSUNG_GALAXY_NOTE_7,
            response.data['device']
        )

    def test_post_tourist_list(self):
        response = self.client.post(
            path=reverse('api_v1__tourist_resource'),
            headers={'Content-type': 'application/json'},
            content_type='application/json',
            data=json.dumps([
                {
                    'first_name': 'Emma',
                    'last_name': 'Goldman',
                    'location': self.BARCELONA_LOCATION,
                    'device': self.SAMSUNG_GALAXY_NOTE_7
                },
                {
                    'first_name': 'Alexander',
                    'last_name': 'Berkman',
                    'location': self.MADRID_LOCATION,
                    'device': self.SAMSUNG_GALAXY_NOTE_7
                }
            ], cls=DjangoJSONEncoder)
        )
        self.assertEqual(response.status_code, 201)
        self.assertDictContainsSubset({
            'first_name': 'Emma',
            'last_name': 'Goldman'
        }, response.data[0])
        self.assertDictContainsSubset(
            self.BARCELONA_LOCATION,
            response.data[0]['location']
        )
        self.assertDictContainsSubset(
            self.BARCELONA_LOCATION,
            response.data[0]['route'][0]
        )
        self.assertDictContainsSubset(
            self.SAMSUNG_GALAXY_NOTE_7,
            response.data[0]['device']
        )
        self.assertDictContainsSubset({
            'first_name': 'Alexander',
            'last_name': 'Berkman'
        }, response.data[1])
        self.assertDictContainsSubset(
            self.MADRID_LOCATION,
            response.data[1]['location']
        )
        self.assertDictContainsSubset(
            self.MADRID_LOCATION,
            response.data[1]['route'][0]
        )
        self.assertDictContainsSubset(
            self.SAMSUNG_GALAXY_NOTE_7,
            response.data[1]['device']
        )

    def test_patch_tourist_item(self):
        response = self.client.patch(
            path=reverse('api_v1__tourist_resource', kwargs={
                             'id': self.tourist_1
                         }),
            headers={'Content-type': 'application/json'},
            content_type='application/json',
            data=json.dumps({
                'last_name': ' Woodcock',
                'location': self.BILBAO_LOCATION
            }, cls=DjangoJSONEncoder)
        )
        self.assertEqual(response.status_code, 202)
        self.assertDictContainsSubset({
            'id': self.tourist_1,
            'first_name': 'George',
            'last_name':  'Woodcock'
        }, response.data)
        self.assertDictContainsSubset(
            self.BILBAO_LOCATION,
            response.data['location']
        )
        self.assertDictContainsSubset(
            self.BARCELONA_LOCATION,
            response.data['route'][0]
        )
        self.assertDictContainsSubset(
            self.BILBAO_LOCATION,
            response.data['route'][1]
        )

    def test_patch_tourist_list(self):
        response = self.client.patch(
            path=reverse('api_v1__tourist_resource'),
            headers={'Content-type': 'application/json'},
            content_type='application/json',
            data=json.dumps([
                {
                    'id': self.tourist_1,
                    'first_name': 'Emma',
                    'last_name': 'Goldman',
                    'location': self.BILBAO_LOCATION
                },
                {
                    'id': self.tourist_2,
                    'first_name': 'Alexander',
                    'last_name': 'Berkman',
                    'location': self.BILBAO_LOCATION
                }
            ], cls=DjangoJSONEncoder)
        )
        self.assertEqual(response.status_code, 202)
        self.assertDictContainsSubset({
            'id': self.tourist_1,
            'first_name': 'Emma',
            'last_name':  'Goldman'
        }, response.data[0])
        self.assertDictContainsSubset(
            self.BILBAO_LOCATION,
            response.data[0]['location']
        )
        self.assertDictContainsSubset(
            self.BARCELONA_LOCATION,
            response.data[0]['route'][0]
        )
        self.assertDictContainsSubset(
            self.BILBAO_LOCATION,
            response.data[0]['route'][1]
        )
        self.assertDictContainsSubset({
            'id': self.tourist_2,
            'first_name': 'Alexander',
            'last_name': 'Berkman'
        }, response.data[1])
        self.assertDictContainsSubset(
            self.BILBAO_LOCATION,
            response.data[1]['location']
        )
        self.assertDictContainsSubset(
            self.MADRID_LOCATION,
            response.data[1]['route'][0]
        )
        self.assertDictContainsSubset(
            self.BILBAO_LOCATION,
            response.data[1]['route'][1]
        )
