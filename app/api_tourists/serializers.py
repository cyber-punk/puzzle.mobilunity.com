# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import serializers

from app.common.serializers import BaseSerializer, IdSerializer


class GeoPointSerializer(BaseSerializer):
    lon = serializers.DecimalField(max_digits=9, decimal_places=6,
                                   min_value=0, max_value=180)
    lat = serializers.DecimalField(max_digits=8, decimal_places=6,
                                   min_value=-90, max_value=90)


class DeviceSerializer(serializers.Serializer):
    phone = serializers.CharField(max_length=20)
    imei = serializers.CharField(max_length=16)
    mac = serializers.CharField(max_length=17)
    manufacture = serializers.CharField(max_length=20)
    model = serializers.CharField(max_length=40)


class TouristSerializer(IdSerializer):
    first_name = serializers.CharField(max_length=40)
    last_name = serializers.CharField(max_length=40)
    device = DeviceSerializer(required=False, many=False)
    location = GeoPointSerializer(many=False)
    route = GeoPointSerializer(required=False, many=True)
