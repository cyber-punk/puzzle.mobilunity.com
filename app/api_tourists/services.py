# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.common.services.mongo_storage import MongoStorageService
from app.api_tourists.models import Tourist


class TouristStorageService(MongoStorageService):
    model_cls = Tourist

    @classmethod
    def _save(cls, attrs, create_new=False):
        route = attrs.get('route', [])
        if not route or route[-1] != attrs['location']:
            route.append(attrs['location'])
            attrs['route'] = route
        return super(TouristStorageService, cls)._save(attrs, create_new)
