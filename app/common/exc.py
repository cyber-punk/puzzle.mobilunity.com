# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from copy import deepcopy

from rest_framework import status
from rest_framework.exceptions import APIException


class UnexpectedError(APIException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_detail = 'Unexpected error'


class ResourceError(APIException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_detail = 'Resource error'

    def __init__(self, *args, **kwargs):
        super(ResourceError, self).__init__(*args)
        self.detail = {'error': self.default_detail,
                       'detail': deepcopy(kwargs)}


class DoesNotExist(ResourceError):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = 'Resource does not exist'


class AlreadyExists(ResourceError):
    status_code = status.HTTP_409_CONFLICT
    default_detail = 'Resource already exists'
