# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import mongoengine as db

from app.common.models.base_model import BaseModel


class MongoModel(BaseModel, db.Document):
    meta = {'abstract': True,
            'allow_inheritance': True}

    def __init__(self, **attrs):
        attr_names = [
            attr_name for attr_name in self._fields.keys()
            if not attr_name.startswith('_')
        ]
        BaseModel.__init__(self, attr_names, **attrs)
        db.Document.__init__(self, **attrs)

    @classmethod
    def _get_collection_name(cls):
        return cls._get_table_name()


class IdMongoModel(MongoModel):
    id = db.UUIDField(primary_key=True)

    meta = {'abstract': True,
            'allow_inheritance': True}


class MongoEmbededDocument(BaseModel, db.EmbeddedDocument):
    meta = {'abstract': True,
            'allow_inheritance': True}

    def __init__(self, **attrs):
        attr_names = [
            attr_name for attr_name in self._fields.keys()
            if not attr_name.startswith('_')
            ]
        BaseModel.__init__(self, attr_names, **attrs)
        db.EmbeddedDocument.__init__(self, **attrs)
