# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.forms.models import model_to_dict

from app.common.models.base_model import BaseModel


class DjangoModel(BaseModel, models.Model):

    def __init__(self, **attrs):
        models.Model.__init__(self, **attrs)

    def to_dict(self):
        return model_to_dict(self)


class IdDjangoModel(DjangoModel):
    id = models.UUIDField(primary_key=True)
