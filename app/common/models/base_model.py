# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.common.utils.strings import plural, decapitalize


class BaseModel(object):

    def __init__(self, attr_names=None, **attrs):
        super(BaseModel, self).__init__()
        self._attr_names = attr_names or []
        for attr_name, attr_value in attrs.items():
            if attr_name in self._attr_names:
                setattr(self, attr_name, attr_value)

    def to_dict(self):
        result = dict([
            (attr_name, self._get_attr(attr_name))
            for attr_name in self._attr_names
        ])
        return result

    def _get_attr(self, attr_name):
        attr = getattr(self, attr_name, None)
        if isinstance(attr, BaseModel):
            return attr.to_dict()
        elif isinstance(attr, list):
            return [item.to_dict() for item in attr]
        return attr

    @classmethod
    def _get_table_name(cls):
        return plural(decapitalize(cls.__name__))
