# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from app.common.exc import UnexpectedError
from app.common.serializers import BaseSerializer
from app.common.serializers import PagingParamsSerializer
from app.common.services.base_service import BaseActionService
from app.common.services.base_service import BaseStorageService


class BaseResource(APIView):

    @staticmethod
    def __get_headers(request):
        result = {}
        if 'headers' in request.META:
            for key, value in request.META['headers'].items():
                result[key.lower().replace('-', '_')] = value
        return result

    @classmethod
    def __fill_data(cls, data, request, **kwargs):
        headers = cls.__get_headers(request)
        if isinstance(data, list):
            for item in data:
                item.update(headers)
                item.update(kwargs)
        else:
            data.update(headers)
            data.update(kwargs)
        return data

    @classmethod
    def _get_body_data(cls, request, **kwargs):
        if isinstance(request.data, list):
            data = [item for item in request.data]
        else:
            data = request.data
        data = cls.__fill_data(data, request, **kwargs)
        return data

    @classmethod
    def _get_query_data(cls, request, **kwargs):
        if isinstance(request.query_params, list):
            data = [item for item in request.query_params]
        else:
            data = request.query_params.dict()
        data = cls.__fill_data(data, request, **kwargs)
        return data


class BaseActionResource(BaseResource):
    params_serializer_cls = BaseSerializer
    result_serializer_cls = BaseSerializer
    action = BaseActionService
    result_status = status.HTTP_200_OK

    def _load(self, data):
        params_serializer = self.params_serializer_cls(
            data=data,
            partial=False,
            many=isinstance(data, list)
        )
        if params_serializer.is_valid(raise_exception=True):
            return params_serializer.validated_data

    def _dump(self, data):
        result_serializer = self.result_serializer_cls(
            data=data,
            partial=False,
            many=isinstance(data, list)
        )
        if not result_serializer.is_valid():
            raise UnexpectedError(detail=result_serializer.errors)
        return result_serializer.validated_data

    def post(self, request, **kwargs):
        input_data = self._get_body_data(request, **kwargs)
        validated_data = self._load(input_data)
        processed_data = self.action.do(validated_data)
        result_data = self._dump(processed_data)
        return Response(result_data, status=self.result_status)


class BaseStorageResource(BaseResource):
    data_serializer_cls = BaseSerializer
    filter_serializer_cls = BaseSerializer
    paging_params_serializer_cls = PagingParamsSerializer
    storage_cls = BaseStorageService

    def __init__(self):
        super(BaseStorageResource, self).__init__()
        self.storage = self.storage_cls()

    def get(self, request, **kwargs):
        data = self._get_query_data(request, **kwargs)
        filter_serializer = self.filter_serializer_cls(
            data=data, partial=True, many=False)
        if not filter_serializer.is_valid():
            return Response(filter_serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

        if self.filter_serializer_cls.get_pk(filter_serializer.validated_data):
            storage_data = self.storage.get_item(
                fltr=filter_serializer.validated_data)
            result_serializer = self.data_serializer_cls(
                data=storage_data,
                many=False)
            if not result_serializer.is_valid():
                raise UnexpectedError(detail=result_serializer.errors)
            result_data = result_serializer.validated_data
        else:
            paging_serializer = self.paging_params_serializer_cls(
                data=data, partial=True, many=False, initial=True)
            if not paging_serializer.is_valid():
                return Response(paging_serializer.errors,
                                status=status.HTTP_400_BAD_REQUEST)
            paging_data = {}
            paging_data.update(paging_serializer.validated_data)
            paging_data['total'] = self.storage.get_count(
                filter_serializer.validated_data)
            if paging_data['total']:
                storage_data = self.storage.get_list(
                    fltr=filter_serializer.validated_data,
                    paging=paging_serializer.validated_data)
                result_serializer = self.data_serializer_cls(
                    data=storage_data,
                    many=True)
                if not result_serializer.is_valid():
                    raise UnexpectedError(detail=result_serializer.errors)
                paging_data['items'] = result_serializer.validated_data
            else:
                paging_data['items'] = []
            paging_data['count'] = len(paging_data['items'])
            result_data = paging_data

        return Response(result_data, status=status.HTTP_200_OK)

    def post(self, request, **kwargs):
        data = self._get_body_data(request, **kwargs)
        params_serializer = self.data_serializer_cls(
            data=data, partial=False, many=isinstance(data, list))
        if not params_serializer.is_valid():
            return Response(params_serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

        storage_data = None
        if isinstance(params_serializer.validated_data, dict):
            storage_data = self.storage.create(
                params_serializer.validated_data)
        elif isinstance(params_serializer.validated_data, list):
            storage_data = [
                self.storage.create(item)
                for item in params_serializer.validated_data
            ]

        result_serializer = self.data_serializer_cls(
            data=storage_data,
            many=isinstance(storage_data, list))
        if not result_serializer.is_valid():
            raise UnexpectedError(detail=result_serializer.errors)

        return Response(result_serializer.validated_data,
                        status=status.HTTP_201_CREATED)

    def patch(self, request, **kwargs):
        data = self._get_body_data(request, **kwargs)
        params_serializer = self.data_serializer_cls(
            data=data, partial=True, many=isinstance(data, list))
        if not params_serializer.is_valid():
            return Response(params_serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

        storage_data = None
        if isinstance(params_serializer.validated_data, dict):
            storage_data = self.storage.update(
                pk=self.data_serializer_cls.get_pk(
                    params_serializer.validated_data),
                attrs=params_serializer.validated_data)
        elif isinstance(params_serializer.validated_data, list):
            storage_data = [
                self.storage.update(
                    pk=self.data_serializer_cls.get_pk(item),
                    attrs=item)
                for item in params_serializer.validated_data
            ]

        result_serializer = self.data_serializer_cls(
            data=storage_data,
            many=isinstance(storage_data, list))
        if not result_serializer.is_valid():
            raise UnexpectedError(detail=result_serializer.errors)

        return Response(result_serializer.validated_data,
                        status=status.HTTP_202_ACCEPTED)
