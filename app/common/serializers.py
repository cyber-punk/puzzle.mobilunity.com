# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from uuid import uuid4

from django.conf import settings
from rest_framework import serializers, fields


class BaseSerializer(serializers.Serializer):
    """
    Django Serializer class is abstract,
    because it contains abstract methods create, update.
    So, here we redefine them as do nothing.
    """

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    @classmethod
    def get_pk(cls, data):
        """
        Returns dict of primary key fields
        """
        if not cls.Meta.primary_key:
            return False
        pk_attr_names = cls.Meta.primary_key.intersection(data.keys())
        result = dict([(attr_name, attr_value)
                       for attr_name, attr_value in data.items()
                       if attr_name in pk_attr_names])
        return result

    class Meta(object):
        primary_key = set()
        ordered = True


class IdSerializer(BaseSerializer):
    id = serializers.UUIDField(default=uuid4)

    class Meta(BaseSerializer.Meta):
        primary_key = {'id'}
        fields = ['id']


class PagingParamsSerializer(BaseSerializer):
    limit = serializers.IntegerField(
        min_value=1, max_value=settings.MAX_PAGING_LIMIT,
        default=settings.MAX_PAGING_LIMIT,
        initial=settings.MAX_PAGING_LIMIT)
    offset = serializers.IntegerField(min_value=0, default=0, initial=0)

    def __init__(self, instance=None, data=fields.empty, **kwargs):
        if data is not fields.empty:
            if 'limit' not in data:
                data['limit'] = self.fields['limit'].initial
            if 'offset' not in data:
                data['offset'] = self.fields['offset'].initial
        super(PagingParamsSerializer, self).__init__(instance, data, **kwargs)
