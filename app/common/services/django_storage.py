# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db.utils import IntegrityError

from app.common.services.base_service import BaseStorageService
from app.common.exc import DoesNotExist, AlreadyExists


class DjangoStorageService(BaseStorageService):
    model_cls = models.Model

    @classmethod
    def _get_query(cls, **attrs):
        return cls.model_cls.objects.filter(**attrs)

    @classmethod
    def get_item(cls, fltr):
        item = cls._get_query(**fltr).first()
        if not item:
            raise DoesNotExist(**fltr)
        result = item.to_dict()
        return result

    @classmethod
    def get_list(cls, fltr=None, paging=None):
        fltr = fltr or {}
        query = cls._get_query(**fltr)
        if paging:
            query = query[paging['offset']:paging['offset'] + paging['limit']]
        items = query.all()
        result = [item.to_dict() for item in items]
        return result

    @classmethod
    def get_count(cls, fltr=None):
        fltr = fltr or {}
        result = cls._get_query(**fltr).count()
        return result

    @classmethod
    def delete(cls, fltr=None):
        fltr = fltr or {}
        cls._get_query(**fltr).delete()

    @classmethod
    def _save(cls, attrs, create_new=False):
        fields = [field.name for field in cls.model_cls._meta.fields]
        fields = dict([
            (attr_name, attr_value) for attr_name, attr_value in attrs.items()
            if attr_name in fields
        ])
        item = cls.model_cls(**fields)
        try:
            item.save(force_insert=create_new)
        except IntegrityError:
            raise AlreadyExists(**fields)
        result = item.to_dict()
        return result
