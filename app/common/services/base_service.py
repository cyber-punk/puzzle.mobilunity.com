# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from abc import ABCMeta


class BaseService(object):
    # Abstract parent class for all services
    __metaclass__ = ABCMeta


class BaseActionService(BaseService):

    @classmethod
    def do(cls, data):
        raise NotImplementedError()


class BaseStorageService(BaseService):

    @classmethod
    def get_item(cls, fltr):
        raise NotImplementedError()

    @classmethod
    def get_list(cls, fltr=None, paging=None):
        raise NotImplementedError()

    @classmethod
    def get_count(cls, fltr=None):
        raise NotImplementedError()

    @classmethod
    def delete(cls, fltr):
        raise NotImplementedError()

    @classmethod
    def create(cls, attrs):
        return cls._save(attrs, create_new=True)

    @classmethod
    def update(cls, pk, attrs):
        data = cls.get_item(pk)
        for attr_name, attr_value in attrs.items():
            if attr_name in data:
                data[attr_name] = attr_value
        return cls._save(data, create_new=False)

    @classmethod
    def _save(cls, attrs, create_new=False):
        raise NotImplementedError()
