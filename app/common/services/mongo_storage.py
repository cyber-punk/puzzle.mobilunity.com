# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import mongoengine as db

from app.common.models.mongo_model import MongoModel
from app.common.services.base_service import BaseStorageService
from app.common.exc import DoesNotExist, AlreadyExists


class MongoStorageService(BaseStorageService):
    model_cls = MongoModel

    @classmethod
    def _get_query(cls, **attrs):
        return cls.model_cls.objects(**attrs)

    @classmethod
    def get_item(cls, fltr):
        item = cls._get_query(**fltr).first()
        if not item:
            raise DoesNotExist()
        result = item.to_dict()
        return result

    @classmethod
    def get_list(cls, fltr=None, paging=None):
        fltr = fltr or {}
        query = cls._get_query(**fltr)
        if paging:
            query = query.limit(paging['limit'])
            query = query.skip(paging['offset'])
        items = query.all()
        result = [item.to_dict() for item in items]
        return result

    @classmethod
    def get_count(cls, fltr=None):
        fltr = fltr or {}
        result = cls._get_query(**fltr).count()
        return result

    @classmethod
    def delete(cls, fltr=None):
        fltr = fltr or {}
        cls._get_query(**fltr).delete()

    @classmethod
    def _save(cls, attrs, create_new=False):
        item = cls.model_cls(**attrs)
        try:
            item.save(force_insert=create_new)
        except db.queryset.NotUniqueError:
            raise AlreadyExists()
        result = item.to_dict()
        return result
