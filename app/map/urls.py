# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import patterns, url

from app.map.views import MapPageView


urlpatterns = patterns(
    '',
    url(r'^map/', MapPageView.as_view(), name='map_page'),
)
