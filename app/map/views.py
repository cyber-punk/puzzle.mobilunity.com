# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.views.generic import TemplateView


class MapPageView(TemplateView):
    template_name = 'map.html'

    def get_context_data(self, **kwargs):
        result = super(MapPageView, self).get_context_data(**kwargs)
        result.update({'default_lat': settings.MAP['default']['lat'],
                       'default_lon': settings.MAP['default']['lon']})
        return result
