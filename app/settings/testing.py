# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .basic import *

DEBUG = True
TEST = True

ALLOWED_HOSTS = ['testserver']

MONGODB_DATABASES['default'].update({
    'name': 'mobilunity_test',
    'host': 'mongodb://127.0.0.1:27017'
})


# setup application for using with nosetests
import django
django.setup()
