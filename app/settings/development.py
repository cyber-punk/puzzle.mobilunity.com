# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .basic import *


DEBUG = True

MONGODB_DATABASES['default'].update({
    'name': 'mobilunity_dev',
    'host': 'mongodb://127.0.0.1:27017'
})
