.PHONY: clear install test run shell, import

DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
SHELL=/bin/bash
APP := $(DIR)/app
ENV := $(DIR)/env/bin
PYTHON := $(ENV)/python
PIP := $(ENV)/pip
PIP_INSTALL := $(PIP) install --no-cache-dir --upgrade
NOSE := $(ENV)/nosetests
COVERAGE_CONFIG = $(DIR)/.coveragerc
MIN_COVERAGE = 0%
REPORTS := $(DIR)/reports
COVERAGE_HTML := $(REPORTS)/cover
COVERAGE_XML := $(REPORTS)/coverage.xml
XUNIT_XML := $(REPORTS)/nosetests.xml
PYLINT_CONFIG = $(DIR)/.pylintrc
PYLINT := $(ENV)/pylint --rcfile="$(PYLINT_CONFIG)"
PEP8 := $(ENV)/pep8 --max-line-length=100 --exclude="*/settings/*,*/migrations/*"

RED=\033[0;31m
BOLD_RED=\033[1;31m
GREEN=\033[0;32m
BOLD_GREEN=\033[1;32m
BLUE=\033[0;34m
BOLD_BLUE=\033[1;34m
BOLD=\033[1m
NC=\033[0m

STATUS_ERROR := ${BOLD_RED}✘${NC} Error
STATUS_OK := ${BOLD_GREEN}✔${NC} OK


#########
# clear #
#########

clear-reports:
	@rm -rf "$(REPORTS)"
	@mkdir "$(REPORTS)"

clear-python-binary:
	find . -name '*.py?' -print -delete

clear-python-cache:
	find . -name '__pycache__' -exec rm -rf {} +

clear-temporary:
	find . -name '*~' -print -delete

clear: clear-reports clear-python-binary clear-python-cache clear-temporary


###########
# install #
###########

install-mongo:
	@echo -e "${GREEN}Installing Mongo DB ... ${NC}"; \
	apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927 ; \
	echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.2.list ;\
	apt-get update ; \
	apt-get install -y mongodb-org

install-apt: install-mongo

install-pip:
	@echo -e "${GREEN}Installing setup tools ... ${NC}"; \
	wget "https://bootstrap.pypa.io/get-pip.py" -P "$(DIR)" -q;\
	/usr/bin/python get-pip.py; \
	find . -name 'get-pip*.py*' -print -delete

install-env:
	@echo -e "${GREEN}Installing virtual environment ... ${NC}"; \
	/usr/local/bin/pip install --upgrade virtualenv; \
	rm -rf "$(DIR)/env/"; \
	virtualenv --clear "$(DIR)/env/"

env-activate:
	. $(ENV)/activate

install-python-libs:
	$(PIP_INSTALL) -r "$(DIR)/requirements.txt" ; \
	if [ $$? -eq 0 ]; then \
		echo -e "Installation complete: ${STATUS_OK}"; \
	else \
		echo -e "Installation failed: ${STATUS_ERROR}"; \
	fi;

install: install-apt install-pip install-env env-activate install-python-libs


########
# test #
########

test-unittests:
	@DJANGO_SETTINGS_MODULE="app.settings.testing" \
	NOSE_COVER_CONFIG_FILE="$(COVERAGE_CONFIG)" \
	$(NOSE) \
--with-coverage --cover-min-percentage="$(MIN_COVERAGE)" \
--cover-package="$(APP)" --cover-erase \
--cover-html --cover-html-dir="$(COVERAGE_HTML)" \
--cover-xml --cover-xml-file="$(COVERAGE_XML)" \
--with-xunit --xunit-file="$(XUNIT_XML)" \
--rednose \
	; \
	if [ $$? -eq 0 ]; then \
		echo -e "Unit Tests: ${STATUS_OK}"; \
	else \
		echo -e "Unit Tests: ${STATUS_ERROR}"; \
	fi;

test-pep8:
	@$(PEP8) "$(APP)" ; \
	if [ $$? -eq 0 ]; then \
		echo -e "PEP8: ${STATUS_OK}"; \
	else \
		echo -e "PEP8: ${STATUS_ERROR}"; \
	fi;

test-pylint:
	@$(PYLINT) "$(APP)" ; \
	if [ $$? -eq 0 ]; then \
		echo -e "Pylint: ${STATUS_OK}"; \
	else \
		echo -e "Pylint: ${STATUS_ERROR}"; \
	fi;

test-quality: test-pep8 test-pylint

test: clear-reports test-unittests test-quality


#######
# run #
#######

run:
	@$(PYTHON) "$(DIR)/manage.py" runserver


#########
# shell #
#########

shell:
	@$(PYTHON) "$(DIR)/manage.py" shell
