Python – Full Stack Developer Test assignment

![Logo](mobilunity.png)

Requirements
============

* Ubuntu 16.04
* Python 2.7
* Django 1.9
* Default Bootstrap 3 framework

Problem description
===================

Barcelona 2020 has been predicted to be the most attractive touristic city.
To better handle more significant amounts of tourists, the IT department designed “BCN ID Tourists cards” which would help in smoothing dynamic resources management.
Each tourist will be given an unique ID NFC / WIFI / GPS card which will be updating the tourist department with their current location.
Having such data the tourist department will be distributing more efficient communication resources (such as buses, trams, taxis).
You have been selected to write a backend for such system!

Requirements
============

As a result we expect two separate projects:

Backend
-------

A Django backend app that is being served by a REST API, providing the basic operations for the tourist office (add/update tourist data, and querying for tourists within the system)


Frontend (optional)
-------------------

A frontend app to provide a Web-based interface to handle such operations seamlessly, using the REST API mentioned before.
You can implement it the way you want, based on the platform and language of your choice (for instance, Python/Django- or JavaScript- based)."

Example applications should contain (whenever its applicable):

* necessary models to handle tourist and all related objects
* fixture with an example tourists data
